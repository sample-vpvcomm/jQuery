/**
 * Created by p.vasin
 * Глобальная для всех сайтов генерация событий
 * check from home
 */
/* --- SELECTORS OBJECT --------------------------------------------------------------------------------------- */
var GTMselector = {
    "order_confirm":'#submit-order',
    "shipping_select":'*[data-toggle*="shipping-select"],#delivery-fieldset .legend span',
    "order_update":'button[value="update_qty"]',
    "banner_click":'img[data-banner-id],.slider-banner-container li',
    "product_click":'.products-grid .item a,' +
                    '.listing-item a,' +
                    '.products-grid .item-content a,' +
                    '.masonry-grid-item a',
    "product_add_compare":'input:checkbox[name="compare"]',
    "product_add_basket":'.btn-in-stock,' +
                        '.button-in-cart,' +
                        '.btn-preorder,' +
                        '.btn-cart,' +
                        '.button-in-stock',
    "product_sort":'.product-sort-by li a,' +
                    '.sort-by li a,' +
                    '.sort-by option,' +
                    '#sort-select,' +
                    '#sort-select-menu,' +
                    '.sort li, .sort option',
    "product_remove_basket":'#shopping-cart .remove,' +
                            '#shopping-cart .remove a' +
                            'form#cart .remove,' +
                            '.btn-remove,' +
                            '.remove .btn',
    "basket_clear":'button[value="empty_cart"]',
    "tab_switch":'ul[class*="nav-tabs"] li,' +
                    '#prod_tabs ul.nav li,' +
                    '#accordion h3 span,' +
                    '.nav-tabs a,' +
                    'ul.product-navigation li',
    "social_icon":'.ya-share2__item,' +
                    '.at-share-btn,' +
                    '.social-share li a',
    "user_login":'#btn-register,#btnRecoveryPass, .btn-orange, div[class*="ulogin-"], ' +
                    'a[href*="account/create"], a[href*="customer/account"], .login-form button,' +
                    'a[href*="customer/account/create"], a[href*="customer/account/login"], ' +
                    '#account-popover-wrap .popover-btn, a[href*="account/login"],' +
                    '.header__login .login'
};

/* --- SLIDERS reconfig --------------------------------------------------------------------------------------- */

customEvents.trigger('slider-config-tag-man',
    {
        revolution: ".main-slider-wrap .tp-banner, " +
        ".fullwidthbanner-container .fullwidthbanner-olympus, " +
        ".fullwidthbanner-container .fullwidthbanner-olympus",
        bx: ".mb_slider_container, .mb_container"
    }
);

var antiDouble = 0;

/**
 * регулярное выражение для комплексной очистки строки
 * @type {RegExp}
 */
var trimRegexp = /\n|\*|Sku:|undefined|Арт\.|:/g;

/**
 * глобальная очистка строки
 * @param string
 * @returns {*}
 */
function megaTrimNative(string) {
    if (typeof string === 'string') {
        return string.replace(trimRegexp,'').trim();
    }
    return string;
}

/**
 * глобальная очистка строки для jQuery
 */
jQuery.fn.megaTrim = function() {
    var str = this.text();
    if (typeof str === 'string') {
        return this.text().replace(trimRegexp,'').trim();
    }
    return this;
};

jQuery(document).on('ready pjax:success', function () {

    /**
     * Получение одного значения
     * из нескольких вариантов возможных
     * после выборки по разным селекторам
     * @param arr
     * @returns {string|boolean}
     */
    function oneFromSeveral (arr,thisContext) {
        if (Array.isArray(arr) && arr.length > 0) {
            var topDataSku = jQuery(thisContext).parents('.masonry-grid-item').data('sku');
            var valueTDS = (typeof topDataSku !== 'undefined')
                ? topDataSku
                : 'undefined';
            if (arr.indexOf(valueTDS) === -1) {
                arr.push(valueTDS);
            }
            var arrToString = arr.join('');
            var oneValueClear = megaTrimNative(arrToString);
            return oneValueClear;
        }
        return false;
    }

    /**
     * banner_click
     * клик по баннеру-картинке
     * return: banner_id
     */
    jQuery(GTMselector.banner_click).click(function(){
        var arr = [
            jQuery(this).data('banner-id'),
            jQuery(this).parents('li').find('.tp-bgimg').attr('src'),
            jQuery(this).find('.tp-bgimg').data('src')
        ];
        var banner_id = oneFromSeveral(arr,this);
            banner_id = typeof banner_id !== 'undefined' ? banner_id : 'unknown banner ID';
        customEvents.trigger('banner_click',{banner_id: banner_id});
    });


    /**
     * product_click
     * клик по товару
     * return: prod_id / prod_url
     */
    jQuery(GTMselector.product_click).click(function(){
        var arr1 = [
            jQuery(this).attr('href'),
            jQuery(this).parent().attr('href')
        ];
        var prod_url = oneFromSeveral(arr1,this);
        var arr2 = [
            jQuery(this).parents('.tag-man').data('sku'),
            jQuery(this).parents('.item').find('.sku').text(),
            jQuery(this).parents('.masonry-grid-item').find('.sku').text()
        ];
        var prod_id = oneFromSeveral(arr2,this);
        customEvents.trigger('product_click',{prod_id: prod_id, prod_url: prod_url});
    });

    /**
     * product_add_compare
     * добавить товар в сравнение
     * return: prod_id / is_checked
     */
    jQuery(GTMselector.product_add_compare).click(function(){
        var arr = [
            jQuery(this).parents('.item').find('.sku').text(),
            jQuery(this).parents('.product-shop').find('.sku').text(),
            jQuery(this).parents('.masonry-grid-item').find('.sku').text()
        ];
        var prod_id = oneFromSeveral(arr,this);
        var is_checked = jQuery(this).prop('checked');
        customEvents.trigger('product_add_compare',{prod_id: prod_id,is_checked:is_checked});
    });

    /**
     * product_sort
     * сортировка товаров
     * return: eventContent
     */
    jQuery(GTMselector.product_sort).click(function(event) {
        var eventType = event.type;
        var content;
        switch(eventType) {
            case 'click':
                content = jQuery(this).text();
                break;
            case 'change':
                content = jQuery(this).find('option:selected').text();
                break;
        }
        var eventContent = content;
        customEvents.trigger('product_sort',{eventContent: eventContent});
    });

    /**
     * product_remove_basket
     * удаление товара из корзины
     * return: prod_id
     */
    jQuery(GTMselector.product_remove_basket).click(function(){
        var arr = [
            jQuery(this).parents('.tag-man').data('sku'),
            jQuery(this).prevAll('.product').find('.sku').text(),
            jQuery(this).parents('.row-fluid').find('.product small').text(),
            jQuery(this).parents('.row-fluid').find('.prod-name small').text(),
            jQuery(this).parents('tr').find('.item-msg:contains("Sku")').text()
        ];
        var prodId = oneFromSeveral(arr,this);
        customEvents.trigger('product_remove_basket',{prod_id: prodId});
    });

    /**
     * basket_clear
     * очистка корзины
     * return: nothing
     */
    jQuery(GTMselector.basket_clear).click(function(){
        customEvents.trigger('basket_clear');
    });

    /**
     * order_confirm
     * клик по кнопке "Заказать"
     * return: nothing
     */
    jQuery(GTMselector.order_confirm).click(function(){
        customEvents.trigger('order_confirm');
    });

    /**
     * shipping_select
     * выбор способа доставки
     * return: shipMethod
     */
    jQuery(GTMselector.shipping_select).click(function(){
        var shipMethod = jQuery(this).text();
        customEvents.trigger('shipping_select',{shipMethod: shipMethod});
    });

    /**
     * order_update
     * клик по кнопке "Пересчитать/Обновить"
     * return: nothing
     */
    jQuery(GTMselector.order_update).click(function(){
        customEvents.trigger('order_update');
    });

    /**
     * tab_switch
     * переключение между табами
     * return: eventContent
     */
    jQuery(GTMselector.tab_switch).click(function(){
        var eventContent = jQuery(this, 'a').text();
        eventContent = megaTrimNative(eventContent);
        antiDouble++;
        if (antiDouble == 1) {
            customEvents.trigger('tab_switch',{eventContent: eventContent});
        }
    });

    /**
     * product_add_basket
     * добавление товра в корзину
     * return: prod_id
     */
    jQuery(GTMselector.product_add_basket).click(function(){
        var arr = [
            jQuery(this).parents('.item').find('.sku').text(),
            jQuery(this).parents('.product-shop').find('.sku').text(),
            jQuery('#product-description').find('.sku').text(),
            jQuery(this).parents('.product-description').find('.sku span').text(),
            jQuery(this).parents('.masonry-grid-item').find('.sku').text()
        ];
        var prod_id = oneFromSeveral(arr,this);
        customEvents.trigger('product_add_basket',{prod_id:prod_id});
    });

    /**
     * social_icon
     * клик по иконке соцсетей
     * return: label / url
     */
    jQuery(GTMselector.social_icon).click(function(){
        customEvents.trigger('social_icon',{label: "label", url: "url"});
    });

    /**
     * user_login
     * авторизация пользователя
     * return: nothing
     */
    jQuery(GTMselector.user_login).click(function(){
        customEvents.trigger('user_login');
    });

});