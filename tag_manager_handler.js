/**
 * Created by p.vasin
 * Обработка событий
 */

jQuery(document).on('ready', function() {

    jQuery('body').on('filter-show-blue', function(evt,data) {
        /**
         * Категория: фильтр
         * Действие: показать
         * Ярлык: цена/серия/мощность и т.д.
         */
        var label = data.parentTitle.find('.caption').text().replace(/\n/g,'').trim();
        var categoryUrl = location.pathname.split('/')[3].split('.')[0];
        var eventLabel = typeof label !== 'undefined' ? label : 'unknownFilter';
        var eventAction = typeof categoryUrl !== 'undefined' ? categoryUrl : 'unknownCategory';
        var forPush = {
            "event": "aristos",
            "eventCategory": "filter",
            "eventAction": "show_"+eventAction,
            "eventLabel": eventLabel
        };
        dataLayer.push(forPush);
    });

    // удаление SeenProducts там где нет блока
    var seenBlockAtPage = jQuery('.block.viewed, #accordion-viewed, .block-viewed');
    if (seenBlockAtPage.length<0) {
        var arrMiddle = [];
        jQuery.map(dataLayer["0"].ecommerce.impressions, function(elem, index){
            if (elem.list !== "SeenProducts") {
                arrMiddle.push(elem);
            }
        });
        dataLayer["0"].ecommerce.impressions = arrMiddle;
    }

    function mergeProductsImpressions()
    {
        var cloneImpressionsAll = jQuery.extend(true,{},dataLayer[0].ecommerce.impressions);
        var middleStore = {};
        for (key in cloneImpressionsAll) {
            var keyInside = cloneImpressionsAll[key]['id'];
            middleStore[keyInside] = cloneImpressionsAll[key];
        }
        var addJsonProducts = (typeof window.jsonProducts !== 'undefined') ? {} : window.jsonProducts;
        return jQuery.extend(true,middleStore,addJsonProducts);
    }

    // --- Banner Click Anywhere ------------

    // клик по баннеру
    customEvents.on('banner_click', function(evt,data) {
        var bann_id = data.banner_id;
        var bannerClick = {
            "event": "aristos",
            "eventCategory": "Interactions",
            "eventAction": "click",
            "eventLabel": "banner",
            "ecommerce": {
                "promoClick": {
                    "promotions": {}
                }
            }
        };

        var bannerClickEcommerce = {
            "id": "",
            "name": "",
            "creative": "",
            "position": ""
        };

        var targetBanner = null;
        if (typeof bann_id === 'number' && jsonBanners[bann_id] !== 'undefined') {
            targetBanner = jsonBanners[bann_id];
        } else if (typeof bann_id === 'string') {
            for (key in jsonBanners) {
                if (bann_id === jsonBanners[key]['src']) {
                    targetBanner = jsonBanners[key];
                    break;
                }
            }
        }
        if (targetBanner) {
            bannerClickEcommerce.id = targetBanner.id;
            bannerClickEcommerce.name = targetBanner.name;
            bannerClickEcommerce.creative = targetBanner.creative;
            bannerClickEcommerce.position = targetBanner.position;
        }

        bannerClick.ecommerce.promoClick.promotions = bannerClickEcommerce;
        dataLayer.push(bannerClick);

    });

    // --- CategoryGroup ----------------------------------------------------------------------------------------------

    if (jsonPage.pageCategory === 'CategoryGroup') {

        // добавить товар в сравнение
        customEvents.on('product_add_compare', function(evt,data) {
            var prod_id = data.prod_id;
            var is_checked = data.is_checked;
            var fullJsonProducts = mergeProductsImpressions();
            var compareObject = jQuery.extend(true,{},jsonPage.productAddCompare);
            var product = fullJsonProducts[prod_id];
            if (typeof product !== 'undefined') {
                delete compareObject.ecommerce;
                if (is_checked) {
                    compareObject.eventAction = 'add';
                } else {
                    compareObject.eventAction = 'remove';
                }
                compareObject.eventProductId = prod_id;
                compareObject.eventPosition = fullJsonProducts[prod_id].position;
                compareObject.eventProductPrice = fullJsonProducts[prod_id].price;
                dataLayer.push(compareObject);
            }
        });

        // сортировка товаров
        customEvents.on('product_sort', function(evt,data) {
            var productSort = {
                "event": "aristos",
                    "eventCategory": "Interactions",
                    "eventAction": "click",
                    "eventLabel": "sort",
                    "eventContent": ""
            };
            var eventContent = data.eventContent;
            productSort.eventContent = eventContent;
            dataLayer.push(productSort);
        });
    }

    // --- Checkout -----------------------------------------------------

    if (jsonPage.pageCategory === 'Checkout') {

        function addStepInfo(evt,data){
            var products = dataLayer[0].ecommerce.checkout.products;
            var infoStep = {
                "event":"aristos",
                "eventCategory": "Interactions",
                "eventAction": "checkout step",
                "eventLabel": data.step,
                'ecommerce': {
                    "checkout": {
                        "actionField": {"step": data.step, "option": data.option},
                        "products": products
                    }
                }
            };
            dataLayer.push(infoStep);
        }

        // первый шаг в checkout = step_first
        customEvents.on('step_first', function(evt,data) {
            addStepInfo(evt,data);
        });

        // второй шаг в checkout = step_second
        customEvents.on('step_second', function(evt,data) {
            addStepInfo(evt,data);
        });

        // третий шаг в checkout = step_third
        customEvents.on('step_third', function(evt,data) {
            addStepInfo(evt,data);
        });

        // удаление товара из корзины
        customEvents.on('product_remove_basket', function(evt,data) {
            jsonPage.allBasket.ecommerce.remove.actionField.list = jsonPage.pageCategory;
            var prod_id = data.prod_id;
            var position = 1;
            jQuery.each(jsonPage.allBasket.ecommerce.remove.actionField.products, function(key, val){
                if (typeof val  === 'object') {
                    if (val.id !== prod_id) {
                        delete jsonPage.allBasket.ecommerce.remove.actionField.products[key];
                    } else {
                        jsonPage.allBasket.ecommerce.remove.actionField.products[key].list = jsonPage.pageCategory;
                        jsonPage.allBasket.ecommerce.remove.actionField.products[key].dimension18 = '1';
                        jsonPage.allBasket.ecommerce.remove.actionField.products[key].position = position;
                    }
                    position++;
                }
            });
            dataLayer.push(jsonPage.allBasket);
        });

        // очистка корзины
        customEvents.on('basket_clear', function(evt,data) {
            jQuery.each(jsonPage.allBasket.ecommerce.remove.actionField.products, function(key, val){
                jsonPage.allBasket.ecommerce.remove.actionField.products[key].list = jsonPage.pageCategory;
            });
            dataLayer.push(jsonPage.allBasket);
        });

        // клик по кнопке "Заказать"
        customEvents.on('order_confirm', function(evt,data) {
            var orderConfirm = {
                "event": "aristos",
                    "eventCategory": "Conversions",
                    "eventAction": "click",
                    "eventLabel": "confirmOrder",
                    "eventContent": "successful"
            };
            dataLayer.push(orderConfirm);
        });

        // выбор способа доставки
        customEvents.on('shipping_select', function(evt,data) {
            var shipMethod = data.shipMethod;
            var shippingSelect = {
                "event": "aristos",
                "eventCategory": "Interactions",
                "eventAction": "switch",
                "eventLabel": "deliveryMethod",
                "ecommerce": {
                    "checkout_option": {
                        "actionField": {
                            "option": shipMethod
                        }
                    }
                }
            };
            dataLayer.push(shippingSelect);
        });

        // клик по кнопке "Пересчитать/Обновить" заказ
        customEvents.on('order_update', function(evt,data) {
            // обновление данных корзины в dataLayer из-за pjax
            var inputQty = jQuery('form#cart input:text[name*="cart"]').parents('form .row-fluid');
            var allBasket = jsonPage.allBasket.ecommerce.remove.actionField.products;
            var newDataLayer = dataLayer[0].ecommerce.checkout.products;
            var storeBasket = {};

            jQuery.each(inputQty,function(){
                var item = jQuery(this);
                var sku = item.find('.sku').text().replace(/\n/g, '').trim();
                var value = item.find('input:text[name*="cart"]').val().replace(/\n/g, '').trim();
                storeBasket[sku] = value;
            });

            for (var i=0; i<newDataLayer .length;i++) {
                for (key in newDataLayer[i]) {
                    var sku2 = newDataLayer[i]['id'];
                    newDataLayer[i]['quantity'] = storeBasket[sku2];
                    allBasket[i]['quantity'] = storeBasket[sku2];
                }
            }
            var orderUpdate = {
                "event": "aristos",
                    "eventCategory": "Interactions",
                    "eventAction": "click",
                    "eventLabel": "recountOrder"
            };
            dataLayer.push(orderUpdate);
        });

    }

    // --- ProductPage --------------------------------------------------

    if (jsonPage.pageCategory == 'ProductPage') {

        var socialIcon = {
            "event": "aristos",
                "eventCategory": "Social",
                "eventAction": "click",
                "eventLabel": "",
                "eventContent": ""
        };

        if (jQuery('#ya-share2').length) {
            Ya.share2('ya-share2', {
                hooks: {
                    onshare: function (name) {
                        var share = Ya.share2('ya-share2');
                        var services = share._services;
                        var i,url,label;
                        for (i = 0; i < services.length; i++) {
                            if (services[i].name === name) {
                                label = services[i].title;
                                url = services[i].location;
                            }
                        };
                        socialIcon.eventLabel = label;
                        socialIcon.eventContent = url;
                        dataLayer.push(socialIcon);
                    }
                }
            });
        }

        // переключение между табами
        customEvents.on('tab_switch', function(evt,data) {
            var eventContent = data.eventContent;
            jsonPage.tabSwitch.eventContent = eventContent;
            dataLayer.push(jsonPage.tabSwitch);
        });
    }

    // --- All PAges -----------------------------------------------------

    // добавление товра в корзину
    customEvents.on('product_add_basket', function(evt,data) {
        var curPage = jsonPage.pageCategory;
        var currentProduct, productAddBasket;

        // получаем первые 18 товаров
        var prodsFromDL0 = {};
        // проверка для главной страницы
        if (typeof dataLayer[0].ecommerce !== "undefined" && dataLayer[0].ecommerce.impressions) {
            jQuery.map(dataLayer[0].ecommerce.impressions,function(elem, index){
                prodsFromDL0[elem.id] = elem;
            });
        }

        // ProductPage = CategoryGroup = SearchResults

        if (curPage === 'ProductPage') {
            productAddBasket = jsonPage.productAddBasket;
        } else {
            // все товары
            var fullJsonProds = jQuery.extend(prodsFromDL0,window.jsonProducts);
            var prod_id = data.prod_id;
            currentProduct = fullJsonProds[prod_id];
            if (typeof currentProduct !== 'undefined') {
                productAddBasket = {
                    "event": "aristos",
                    "eventCategory": "Conversions",
                    "eventAction": "add",
                    "eventLabel": "cart",
                    "eventLocation": currentProduct.category, // название блока с товарами
                    "eventPosition": currentProduct.position, // порядковый номер товара в блоке
                    "eventCategoryId": currentProduct.id_cat, // id категории товара
                    "eventProductId": currentProduct.id, // id товара
                    "eventProductPrice": currentProduct.price, // цена товара
                    "ecommerce": {
                        "add": {
                            "actionField": {}
                        }
                    }
                };
                currentProduct.list = curPage;
                currentProduct.quantity = 1;
                currentProduct.coupon = "";
                delete currentProduct.id_cat;
                productAddBasket.ecommerce.add.actionField = currentProduct;
            }
        }
        sendEvent('add-to-cart', productAddBasket);
    });

    // клик по иконке соцсетей
    customEvents.on('social_icon', function(evt,data) {
        var label = data.label;
        var url = data.url;
        jsonPage.socialIcon.eventLabel = label;
        jsonPage.socialIcon.eventContent = url;
        dataLayer.push(jsonPage.socialIcon);
    });

    // авторизация пользователя
    customEvents.on('user_login', function(evt,data) {
        var userLogin = {
            "event": "aristos",
                "eventCategory": "Interactions",
                "eventAction": "click",
                "eventLabel": "signIn"
        };
        dataLayer.push(userLogin);
    });

    // клик по товару
    customEvents.on('product_click', function(evt,data) {
        var prod_id = data.prod_id;
        var prod_url = data.prod_url;
        var product;
        if (typeof window.jsonProducts !== 'undefined' && typeof window.jsonProducts[prod_id] !== 'undefined') {
            product = window.jsonProducts[prod_id];
        } else {
            if(dataLayer[0].hasOwnProperty('ecommerce') === false || dataLayer[0].ecommerce.hasOwnProperty('impressions') === false){
                console.log('Not exists product for SKU: '+prod_id);
                return false;
            }
            var impress = dataLayer[0].ecommerce.impressions;
            jQuery.map(impress, function (elem, index) {
                if (elem.id === prod_id) {
                    product = elem;
                }
            });
        }
        if (typeof product !== 'undefined') {
            if(product.hasOwnProperty('name') === false){
                console.log('Not exists product for SKU: '+prod_id);
                return false;
            }
            var productClick = {
                "event":"aristos",
                "eventCategory": "Interactions",
                "eventAction": "click",
                "eventLabel": "product",
                "ecommerce": {
                    "click": {
                        "actionField": {"list": jsonPage.pageCategory ? jsonPage.pageCategory : "Unknown"},
                        'products': [{
                            "name": product.name,
                            "id": product.id,
                            "price": product.price,
                            "brand": product.brand,
                            "category": product.category,
                            "variant": product.variant,
                            "position": product.position
                        }]
                    }
                }
            };
            dataLayer.push(productClick);
        }
    });
});
