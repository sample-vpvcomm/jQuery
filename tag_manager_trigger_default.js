/**
 * Created by p.vasin
 * Base Default
 * Генерация событий
 */


/* --- SLIDERS reconfig --------------------------------------------------------------------------------------- */

customEvents.trigger('slider-config-tag-man',
    {
        revolution: ".fullwidthbanner-container .fullwidthbanner-olympus",
    }
);


/* --- NEW CONFIG TAG MANAGER for global.js VIA TRIGGER --------------------------------------------------------- */

/**
 * configTagMan.selector.widget.products = parent - sky - shown - pill
 * configTagMan.selector.widget.banners = click.selector - viewport.selector
 */

var newConfigTagMan = {
    "selector":{
        "widget":{
            "products":{
                "key1":"value1"
            },
            "banners":{
                "click":{
                    "key2":"value2"
                },
                "viewport":{
                    "key3":"value3"
                }
            }
        }
    }
};

customEvents.trigger('config-tag-man',{addConfig:newConfigTagMan});


jQuery(document).on('ready pjax:success', function () {

    /* --- BANNER CLICK ------------------------------------------------------------------------------------------ */

    // общая функция триггер для нескольких выборок кликов по баннеру
    function generateTriggerBannerClick(banner_id)
    {
        customEvents.trigger('banner_click',{banner_id: banner_id});
    }

    // клик по баннеру-ссылке в слайдере Revolution
    jQuery('.tp-revslider-mainul a').click(function(){
        var banner_id = jQuery(this).parents('li').find('.tp-bgimg').attr('src');
        generateTriggerBannerClick(banner_id);
    });

    // клик по баннеру-картинке
    jQuery('img[data-banner-id]').click(function(){
        var banner_id = jQuery(this).data('banner-id');
        generateTriggerBannerClick(banner_id);
    });

    /* --- EVENTS -------------------------------------------------------------------------------------------------- */

    // клик по товару
    jQuery('.products-grid a img, .products-grid h3 a').click(function(){
        var prod_id = jQuery(this).parents('li').data('sku');
        var prod_url_text = jQuery(this).attr('href');
        var prod_url_img = jQuery(this).parent().attr('href');
        var prod_url = typeof prod_url_text !== 'undefined' ? prod_url_text : prod_url_img;
        customEvents.trigger('product_click',{prod_id: prod_id, prod_url: prod_url});
    });

    // добавить товар в сравнение
    jQuery('input:checkbox[name="compare"]').click(function(){
        var prod_id = jQuery(this).parents('li').data('sku');
        var is_checked = jQuery(this).prop('checked');
        customEvents.trigger('product_add_compare',{prod_id: prod_id,is_checked:is_checked});
    });


    // сортировка товаров
    jQuery('.sort-by').on('change', function() {
        var eventContent = jQuery(this).find('option:selected').text().replace(/\n/g,'').trim();
        customEvents.trigger('product_sort',{eventContent: eventContent});
    });


    // удаление товара из корзины
    jQuery('.btn-remove').click(function(){
        var prodID = jQuery(this).parents('tr').find('.item-msg:contains("Sku")').text();
        var prodIdClear = prodID.replace(/\n/g,'').replace('* Sku: ','').trim();
        customEvents.trigger('product_remove_basket',{prod_id: prodIdClear});
    });


    // очистка корзины = basket_clear = !!!
    jQuery('button[value="empty_cart"]').click(function(){
        customEvents.trigger('basket_clear');
    });


    // клик по кнопке "Заказать"
    jQuery('#submit-order').click(function(){
        customEvents.trigger('order_confirm');
    });


    // выбор способа доставки
    jQuery('*[data-toggle*="shipping-select"]').click(function(){
        var shipMethod = jQuery(this).text().replace(/\n/g, '').trim();
        customEvents.trigger('shipping_select',{shipMethod: shipMethod});
    });


    // клик по кнопке "Пересчитать/Обновить" заказ
    jQuery('button[value="update_qty"]').click(function(){
        customEvents.trigger('order_update');
    });


    // переключение между табами
    jQuery('#accordion h3 span').click(function(){
        var eventContent = jQuery(this, 'a').text().replace(/\n/g, '').trim();
        customEvents.trigger('tab_switch',{eventContent: eventContent});
    });

    // добавление товра в корзину
    jQuery('.btn-cart, .button-in-stock').click(function(){
        var prod_id_1 = jQuery(this).parents('li').data('sku');
        var prod_id_2 = jQuery(this).parents('.product-shop').data('sku');;
        var prod_id = typeof prod_id_1 !== "undefined" ? prod_id_1 : prod_id_2;
        customEvents.trigger('product_add_basket',{prod_id:prod_id});
    });

    // авторизация пользователя
    jQuery('.login-form button').click(function(){
        customEvents.trigger('user_login');
    });

});