/**
 * Created by p.vasin on 24.03.17.
 */

// таймер
var gtmPushTimerProducts,gtmPushTimerBanners, gtmBxPushTimer = null;
// таймер для цикличности пушинга
var gtmPushTimerCycle = 2000;
// проверка фокусировки браузера
var gtmFocusedBrowser = true;
window.onfocus = function() {
    gtmFocusedBrowser = true;
};
window.onblur = function() {
    gtmFocusedBrowser = false;
};
// обозначение баннеров из слайдеров
var gtmCreativeForSlider = "slider";

// накопитель просмотренных продуктов
var gtmImpressProducts = {};
// шаблон объекта для пуша в dataLayer
function gtmReturnEventObject(impressions){
    var result = {
        "event":"aristos",
        "eventCategory": "Non-Interactions",
        "eventAction": "show",
        "eventLabel": "products",
        "ecommerce":{
            "impressions": impressions
        }
    };
    return result;
}
// шаблон объекта просмотренных баннеров для пуша
function gtmReturnPromoViewObject () {
    var promo = {
        "event":"aristos",
        "eventCategory": "Non-Interactions",
        "eventAction": "show",
        "eventLabel": "banners",
        "ecommerce":{
            "promoView": {
                "promotions": []
            }
        }
    };
    return promo;
}

function gtmReturnPromotionObject () {
    var promo = {
        "id": "",
        "name": "",
        "creative": "",
        "position": ""
    };
    return promo;
}

// селектор для выборки
var configSelectorProducts = configTagMan.selector.widget.products;

// проверка элемента на видимость во viewport
function gtmCheckElementInViewport(el,screen)
{
    if (screen === undefined) {
        screen = gtmAboutScreen();
    }
    var element = gtmAboutElement(el);

    var screenWidth = screen.screenWidth;
    var screenHeight = screen.screenHeight;
    var scrollTop = screen.scrollTop;
    var scrollLeft = screen.scrollLeft;

    var seeX1 = scrollLeft;
    var seeX2 = screenWidth + scrollLeft;
    var seeY1 = scrollTop;
    var seeY2 = screenHeight + scrollTop;


    var elemTop = element.elemTop;
    var elemLeft = element.elemLeft;
    var elemWidth = element.elemWidth;
    var elemHeight = element.elemHeight;

    var elemX1 = elemLeft;
    var elemX2 = elemLeft + elemWidth;
    var elemY1 = elemTop;
    var elemY2 = elemTop + elemHeight;

    if( elemX1 >= seeX1 && elemX2 <= seeX2 && elemY1 >= seeY1 && elemY2 <= seeY2 ){
        return true;
    }

    return false;
}

// сбор данных об экране
function gtmAboutScreen () {

    // габариты экрана
    var screenWidth = jQuery(window).width();
    var screenHeight = jQuery(window).height();
    var scrollTop = jQuery(document).scrollTop();
    var scrollLeft = jQuery(document).scrollLeft();

    // координаты углов видимой области
    var seeX1 = scrollLeft;
    var seeX2 = screenWidth + scrollLeft;
    var seeY1 = scrollTop;
    var seeY2 = screenHeight + scrollTop;

    return {
        "screenWidth":screenWidth,
        "screenHeight":screenHeight,
        "scrollTop":scrollTop,
        "scrollLeft":scrollLeft,
        "seeX1":seeX1,
        "seeX2":seeX2,
        "seeY1":seeY1,
        "seeY2":seeY2
    };

};

// сбор данных об элементе
function gtmAboutElement (element) {

    // параметры элемента
    var positionElement = jQuery(element).offset();
    var elemTop = positionElement.top;
    var elemLeft = positionElement.left;
    var elemWidth = jQuery(element).width();
    var elemHeight = jQuery(element).height();

    // координаты элемента
    var elemX1 = elemLeft;
    var elemX2 = elemLeft + elemWidth;
    var elemY1 = elemTop;
    var elemY2 = elemTop + elemHeight;

    return {
        "positionElement":positionElement,
        "elemTop":elemTop,
        "elemLeft":elemLeft,
        "elemWidth":elemWidth,
        "elemHeight":elemHeight,
        "elemX1":elemX1,
        "elemX2":elemX2,
        "elemY1":elemY1,
        "elemY2":elemY2
    };
}

// вычисление позиции товара во viewport
function gtmCheckProdPosition(divParent){
    var arr = jQuery(divParent).children();
    var screen = gtmAboutScreen();
    jQuery.each(arr, function () {
        var item = jQuery(this);
        var checkViewport = gtmCheckElementInViewport(item,screen);
        if (checkViewport) {
            var sku = item.find(configSelectorProducts.sku).text().replace(/\n/g, '').trim();
            var descr = jsonProducts[sku];
            if (typeof descr !== 'undefined') {
                gtmImpressProducts[sku] = descr;
                clearTimeout(gtmPushTimerProducts);
                gtmPushTimerProducts = setTimeout(function(){
                    var result = [];
                    var object = gtmImpressProducts;
                    for (var key in object) {
                        var value = object[key];
                        if (typeof value !== 'undefined') {
                            result.push(value);
                        }
                    }
                    var chunkSize = 18;
                    while (result.length > 0) {
                        var forPush = gtmReturnEventObject(result.splice(0, chunkSize));
                        dataLayer.push(forPush);
                    }
                }, gtmPushTimerCycle);
            }
        }

    });
}

// перебор товаров из виджетов
function gtmIterateWidget(array){
    var result = [];
    var pageType = dataLayer[0]["pageType"];
    var middleStore = {};
    if (typeof jsonSellProducts !== 'undefined') {
        jQuery.extend(true,jsonProducts,jsonSellProducts);
    }
    jQuery.each(array,function(){
        var item = jQuery(this);
        // получение названия виджета
        var itemH2 = item.parents('.tab-content').prevUntil('.widget').prev().find('h2').text();
        var itemSku = item.find(configSelectorProducts.sku).text().replace(/\n/g, '').trim();
        var itemDataSku = item.data('sku');
        if (typeof itemDataSku !== 'undefined') {
            itemSku = itemDataSku;
        }
        itemSku = itemSku.toString().replace(/Арт./gi,''); // удаление префикса для Rowenta

        var descr = jsonProducts[itemSku];
        if (typeof descr !== 'undefined') {
            if (jQuery.isPlainObject(descr) && descr.hasOwnProperty('list')) {
                descr.list = (typeof itemH2 !== 'undefined' && itemH2 !== '') ? itemH2 : pageType;
            }
            // новое получение названия виджета из атрбута data-widget-name шаблона widget/grid
            var widget_name = item.parents('ul,.masonry-grid-fitrows').data('widget-name');
            if (typeof widget_name !== 'undefined') {
                descr.list = widget_name;
            }
            middleStore[itemSku] = descr;
        }

    });
    for (var key in middleStore) {
        var value = middleStore[key];
        if (typeof value !== 'undefined') {
            result.push(value);
        }
    }
    var forPushWidget = gtmReturnEventObject(result);
    dataLayer.push(forPushWidget);
}

function gtmCheckBannerPosition(bannerSet)
{
    var arr = bannerSet;
    var screen = gtmAboutScreen();
    // хранилище баннеров во вьювпорте
    var middleStore = {};
    clearTimeout(gtmPushTimerBanners);
    gtmPushTimerBanners = setTimeout(function(){

        var clone = gtmReturnPromoViewObject ();

        jQuery.each(arr, function () {
            var item = jQuery(this);
            var bannerID = item.data('banner-id');
            var checkViewport = gtmCheckElementInViewport(item,screen);
            if (checkViewport && jsonBanners[bannerID] !== 'undefined') {
                var bannerForPush = jsonBanners[bannerID];
                middleStore[bannerID] = bannerForPush;
            };
        });

        jQuery.map(middleStore, function(elem, index){
            if (typeof elem === "object") {
                delete elem["src"];
                clone.ecommerce.promoView.promotions.push(elem);
            }
        });

        // пушим только если есть что пушить
        if (clone.ecommerce.promoView.promotions.length > 0) {
            dataLayer.push(clone);
        }

    }, gtmPushTimerCycle);
}


function gtmPushOneBanner(bannerObj) {
    var promoObject = gtmReturnPromoViewObject();
    if (typeof bannerObj !== 'undefined' && gtmFocusedBrowser) {
        delete bannerObj.src;
        promoObject.ecommerce.promoView.promotions.push(bannerObj);
        dataLayer.push(promoObject);
    }
}

function bxSliderRecord (selector) {

    if (typeof selector === 'undefined' && typeof window.bxSliderObject !== 'undefined') {
        selector = window.bxSliderObject.selector;
    }

    var bxImages = jQuery(selector + " img:visible[data-banner-id]").length > 0 ? jQuery(selector + " img:visible[data-banner-id]") : jQuery(selector + " img:visible[data-banner]");
    var screen = gtmAboutScreen();

    // учет просмотра баннера после клика
    jQuery('.bx-pager-item').on('click', function () {
        if(window.bxSliderObject !== undefined) {
            var slider = window.bxSliderObject;
            var current = slider.getCurrentSlide();
            slider.goToSlide(current);
        }
    });

    if (typeof bxImages !== 'undefined' && bxImages.length > 0) {

        jQuery('body').on('bx-onSliderLoad', function(evt,data) {
            clearTimeout(gtmBxPushTimer);
            gtmBxPushTimer = setTimeout(function(e){

                var bannerIndex = data.index;
                var showImgSlide = gtmCheckElementInViewport(bxImages[bannerIndex],screen);
                if (showImgSlide) {
                    var banner = jQuery(bxImages[bannerIndex]);
                    var bannerID = banner.data('banner-id') ? banner.data('banner-id') : banner.data('banner');
                    var bannerObj = jsonBanners[bannerID];
                    var objectLength = Object.keys(bannerObj).length;
                    if (objectLength > 0) {
                        bannerObj.creative = gtmCreativeForSlider;
                        gtmPushOneBanner(bannerObj);
                    }
                }
            }, gtmPushTimerCycle);
        });

        jQuery('body').on('bx-onSlideAfter', function(evt,data) {
            clearTimeout(gtmBxPushTimer);
            gtmBxPushTimer = setTimeout(function(e){
                var slideElement = jQuery(data.slideElement);
                var showImgSlide = gtmCheckElementInViewport(data.slideElement,screen);
                if (showImgSlide) {
                    var imgSlide = slideElement.find('img'),
                        imgSlideID = imgSlide.data('banner-id') ? imgSlide.data('banner-id') : imgSlide.data('banner');
                    var bannerObj = jsonBanners[imgSlideID];
                    var objectLength = Object.keys(bannerObj).length;
                    if (objectLength > 0) {
                        bannerObj.creative = gtmCreativeForSlider;
                        gtmPushOneBanner(bannerObj);
                    }
                }
            }, gtmPushTimerCycle);

        });
    }
}


jQuery(document).on('ready', function(){
    // учет баннеров из bx-слайдера
    bxSliderRecord();
    // родитель для продуктов
    var gtmWrapProds = configSelectorProducts.parent;
    // для главной страницы из виджетов вибираем только видимые
    if (jsonPage.pageCategory === "Main") {
        gtmWrapProds = gtmWrapProds + ":visible";
    }
    // выборка статичных баннеров
    var bannersVisible = jQuery(".m-banner img:visible[data-banner-id], .widget-banner img:visible[data-banner-id], .action-block img:visible[data-banner-id]");
    // выборка баннеров из bx-слайдера
    var sliderBX = jQuery(".widget-slider img:visible[data-banner-id]");
    // учет показов баннеров в слайдере Revolution
    var sliderRevolutionHome = window.sliderRevolutionHome;
    if (typeof sliderRevolutionHome !== 'undefined' && sliderRevolutionHome.length) {
        var revapi = sliderRevolutionHome.show().revolution({
            viewPort: {
                enable: true,
                outof: 'pause',
                visible_area: '50%',
                presize: true
            }
        });
        revapi.on('revolution.slide.onchange', function(event, data) {
            console.log('revolution slide onchange');
            var currentSlide = data.slide.find('.tp-bgimg');
            var bannID = currentSlide.attr('src');
            var screen = gtmAboutScreen();
            var showImgSlide = gtmCheckElementInViewport(data.slide,screen);
            var targetBanner = null;
            for (key in jsonBanners) {
                if (bannID === jsonBanners[key]['src']) {
                    var oneSlide = gtmReturnPromotionObject();
                    targetBanner = jsonBanners[key];
                    oneSlide.id = targetBanner.id;
                    oneSlide.name = targetBanner.name;
                    oneSlide.creative = gtmCreativeForSlider;
                    oneSlide.position = targetBanner.position;
                }
            }
            // уникальный пушинг каждого просмотра если баннер во вьювпорте
            if (showImgSlide && typeof oneSlide !== 'undefined') {
                gtmPushOneBanner(oneSlide);
            }
        });
    };
    // на первичную загрузку страницы
    gtmCheckProdPosition(gtmWrapProds);
    gtmCheckBannerPosition(bannersVisible);
    jQuery(document).on('scroll',function(){
        bxSliderRecord();
        gtmCheckProdPosition(gtmWrapProds);
        gtmCheckBannerPosition(bannersVisible);
    });
    jQuery(window).on('resize',function(){
        bxSliderRecord();
        gtmCheckProdPosition(gtmWrapProds);
        gtmCheckBannerPosition(bannersVisible);
    });
    // учет товаров из виджетов только там где они есть
    if (jQuery(".widget, #upsell-product-table, .block-related").length>0) {
        var shownProds = jQuery(configSelectorProducts.shown);
        gtmIterateWidget(shownProds);
        jQuery('.nav-pills a').on('shown.bs.tab', function (e) {
            var pill = jQuery(this);
            var needTab = pill.parents('ul.nav-pills').next('.tab-content');
            var shownProdsPill = needTab.find(configSelectorProducts.pill);
            gtmIterateWidget(shownProdsPill);
        });
    }
});